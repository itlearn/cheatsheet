# cheatcheet

## ref

cheatsheets from:

- opensource.com
- redhat developer
- packetlife.net

## github repo

Christian's "Cheat-Sheets"

```bash

git clone https://github.com/ChristianLempa/cheat-sheets

```
